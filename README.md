# Joe's Python Notes
## What is this?

The purpose of this repository is for me to store various concepts executed in python, so I don't have to keep relearning them.

## Contents

* 3D Point Cloud Rotation about X, Y, and Z Axis
  * [Wikipedia Article](https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations)
  * <img src="rotation3d/rotation.gif" width="600">
* DBSCAN Clustering Algorithm
  * [Wikipedia Article](https://en.wikipedia.org/wiki/DBSCAN)
  * 3D implementation of the demo found at [scikit-learn.org](http://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html)
  * <img src="dbscan/DBSCAN_3d.png">
* Image Processing
  * Background Subtraction
    * [Wikipedia Article](https://en.wikipedia.org/wiki/Background_subtraction)
    * <img src="imageProcessing/background_subtraction.png" width="600">
