import pyforms
import matplotlib.pyplot as plt
import numpy as np
from pyforms import BaseWidget

from pyforms.controls import ControlImage
from pyforms.controls import ControlEmptyWidget
from pyforms.controls import ControlButton
from pyforms.controls import ControlMatplotlib
class ImageViewer(BaseWidget):

    def __init__(self):
        super(ImageViewer,self).__init__('Image Viewer')

        self._image =  ControlImage()
        self._button = ControlButton('load image')
        self._button.value = self.__load_image
        self._button2 =  ControlButton('test')
        self._button2.value = self.__load_image2
        self._plot = ControlMatplotlib()
        self.formset = [{
            'Tab1':('_image',['_button','_button2']),
            'Tab2':('_plot','_button2')
        } ]

    def __load_image(self):
            """Button action event"""
            self._image.value = 'images/beau.jpg'

    def __load_image2(self):
            print(np.load('images/beau.jpg'))
if __name__ == "__main__":   pyforms.start_app( ImageViewer )
