import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft, fftfreq, fftshift

nSamples = 600
samplePeriod = 1.0/800.0

xs = np.linspace(0, nSamples*samplePeriod, nSamples)

signalComponents = [
    {
    'amplitude':1,
    'frequency':50,
    'phase':0,
        },
    {
    'amplitude':0.5,
    'frequency':80,
    'phase':0,
        },
    {
    'amplitude':0,
    'frequency':0,
    'phase':0,
        },
    ]

ys = np.zeros(nSamples)

for signal in signalComponents:
    ys += signal['amplitude']*np.cos(2*np.pi*signal['frequency']*xs + signal['phase'])

yf = fft(ys)
xf = np.linspace(0.0,1.0/(2.0*samplePeriod),nSamples//2)
plt.plot(xf, 2.0/nSamples * np.abs(yf[0:nSamples//2]))
plt.grid()
plt.show()
