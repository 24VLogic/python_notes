import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def setup_virtual_space():
    virt = np.zeros((160,320))
    virt += 300
    virt[80:160,60:90] = 40
    virt[120:160,210:280] = 120
    return virt
def v_disparity(img):
    histWidth = int(np.max(img))+1
    vDisparity = np.zeros((img.shape[0],histWidth))

    for i,row in enumerate(img):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        vDisparity[i] = hist
    return vDisparity

def u_disparity(img):
    histWidth = int(np.max(img))+1
    uDisparity = np.zeros((img.T.shape[0],histWidth))

    for i,row in enumerate(img.T):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        uDisparity[i] = hist
    return uDisparity.T

def main():
    virt = setup_virtual_space()
    vDisp = v_disparity(virt)
    uDisp = u_disparity
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.imshow(vDisp)
    plt.show()

if __name__ == "__main__":
    main()
