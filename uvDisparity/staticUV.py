import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import animation
import matplotlib.cm as cm

def v_disparity(img):
    histWidth = int(np.max(img))+1
    vDisparity = np.zeros((img.shape[0],histWidth))

    for i,row in enumerate(img):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        vDisparity[i] = hist
    return vDisparity.astype('uint16')

def u_disparity(img):
    histWidth = int(np.max(img)+1)
    uDisparity = np.zeros((img.T.shape[0],histWidth))

    for i,row in enumerate(img.T):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        uDisparity[i] = hist
    uDisparity.T[0]+=1
    return uDisparity.T.astype('uint16')


def simulated_image():
    image = np.zeros((16,16))
    for i, imageRow in enumerate(image[int(image.shape[0]/2):image.shape[0]+1]):
        imageRow += int(image.shape[0]/2) - i
        #print(int(image.shape[0]/2) - i)
        image[i] += 8
    for i in range(4,12):
        for j in range(4,image.shape[1]-4):
            image[i,j] = 4
    return(image.astype('uint16'))

def v_rectangle(inspect,image):
    rectangle = Rectangle(
        (0-0.4,inspect-.5),
        width = image.shape[1]-0.2,
        height = 1,
        facecolor='none',
        edgecolor='w'
            )
    vRectangle = Rectangle(
        (0-0.4,inspect-.5),
        width = image.max()+0.7,
        height = 1,
        facecolor='none',
        edgecolor='w'
            )
    return(rectangle,vRectangle)

def u_rectangle(inspect,image):
    rectangle = Rectangle(
        (inspect,image.shape[0]+0),
        width = 1,
        height = image.shape[1]-1,
        facecolor='none',
        edgecolor='w'
            )
    return(rectangle)
def v_hist(image,inspect,histImage):
    row = image[inspect]
    n, bins, patches = histImage.hist(row, range(row.max().astype('uint16')+1))
    colors = cm.viridis(np.linspace(0, 2, int(image.shape[1]+1)))

    for patchNumber, patch in enumerate(patches):
        patch.set_color(colors[int(n[patchNumber])])


def static_uv(inspectionPoint):
    #Function will create static uv display at inspectionPoint
    inspect = inspectionPoint

    #Create simulated image
    image = simulated_image()
    #Compute V disparity map of simulated image
    vDisp = v_disparity(image)
    #Compute rectangle highlights for image and V disparity map

    rect,vRect = v_rectangle(inspect,image)

    #vDisp[0:inspect] = 0
    #Set colormap
    colors = cm.viridis(np.linspace(0, 2, int(image.shape[1]+1)))
    #create figure and axes
    fig = plt.figure()
    dImg = fig.add_subplot(222)
    vDispImg = fig.add_subplot(221)
    vHistImg = fig.add_subplot(223)
    vHistImg.set_xlim(0,image.max())
    vHistImg.set_ylim(0,image.shape[0])
    vHistImg.set_facecolor(colors[0])
    vHistImg.set_aspect(vDispImg.get_aspect())
    dImg.title.set_text("Distance Axis Image")
    vDispImg.title.set_text("V Disparity Map")
    vHistImg.title.set_text("Histogram of Distance Values")
    #show images
    dImg.imshow(image,cmap = 'viridis_r')
    vDispImg.imshow(vDisp,cmap='viridis')
    #compute histogram
    v_hist(image,inspect,vHistImg)
    #add highlight rectangles
    dImg.add_patch(rect)
    vDispImg.add_patch(vRect)
    plt.tight_layout()


def numerical_vDisp():
    mat = np.random.randint(5,size = (5,5))
    vDisp = v_disparity(mat)
    fig = plt.figure()
    ax = fig.add_subplot(111,aspect = 'equal')
    ax.set_xlim(0,mat.shape[1])
    ax.set_ylim(0,mat.shape[0])
    ax.invert_yaxis()
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    #ax.set_xticks([])
    #ax.set_yticks([])
    ax.grid(color='k',linewidth = 2)
    print(mat)
    matNumbers = {}
    for y,row in enumerate(mat):
        for x,d in enumerate(row):
            matNumbers[(x,y)] = d


    for (x,y),d in matNumbers.items():

        plt.text(x+0.5,y+0.7,str(d),horizontalalignment = 'center',verticalalignment = 'center',fontsize = 200/mat.shape[0])
        #print(x,y,d)
    #print(vDisp)

def matrix_to_text(matrix):
    ax = plt.gca()
    ax.set_xlim(0,mat.shape[1])
    ax.set_ylim(0,mat.shape[0])
    ax.invert_yaxis()
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_xticks(range(mat.shape[0]))
    ax.set_yticks(range(mat.shape[1]))
    ax.grid(color='k',linewidth = 2,alpha = 0.5)
    matNumbers = {}
    for y,row in enumerate(matrix):
        for x,d in enumerate(row):
            matNumbers[(x,y)] = d

    for (x,y),d in matNumbers.items():

        plt.text(x+0.5,y+0.6,str(d),horizontalalignment = 'center',verticalalignment = 'center',fontsize = 100/mat.shape[0])

def draw_hist_and_rectangle(histAxis,imageAx,vDispImageAx,inspectionPoint,image):
    rect = Rectangle(
        (0,inspectionPoint),
        height = 1,
        width = imageAx.get_xlim()[1],
        facecolor = 'none',
        edgecolor = 'b',
        linewidth=2,
        linestyle = '-')
    vRect = Rectangle(
        (0,inspectionPoint),
        height = 1,
        width = vDispImageAx.get_xlim()[1],
        facecolor = 'none',
        edgecolor = 'b',
        linewidth=2,
        linestyle = '-')
    imageAx.add_patch(rect)
    vDispImageAx.add_patch(vRect)
    histAxis.hist(image[inspectionPoint],range(int(image.max())+2),color='b',rwidth = 0.5)

fig = plt.figure()
ax = fig.add_subplot(222,aspect = 'equal')
ax.title.set_text("Distance Image Matrix")
ax.set_ylabel('y axis')
ax.set_xlabel('x axis')
mat = np.random.randint(5,size = (5,5))
#mat = simulated_image()
#numerical_vDisp()
matrix_to_text(mat)
ax2 = fig.add_subplot(221,aspect ='equal')
ax2.title.set_text("V Disparity Matrix")
ax2.set_ylabel('y axis')
ax2.set_xlabel('Distance axis')
vDisp = v_disparity(mat)
matrix_to_text(vDisp)
ax2.set_xlim(0,mat.max()+1)
ax3 = fig.add_subplot(223,aspect = 'auto')
ax3.grid(color='k',linewidth = 2,alpha = 0.5)
ax3.set_yticks(range(int(mat.shape[0])))
ax3.set_ylim(0,int(mat.shape[0]))
ax3.set_xticks(range(int(mat.shape[0])))
ax3.set_xlim(0,mat.max()+1)
ax3.title.set_text('V Disparity Histogram')
ax3.set_ylabel('Frequency')
ax3.set_xlabel('Distance Axis')
draw_hist_and_rectangle(ax3,ax,ax2,3,mat)
plt.tight_layout()
plt.savefig('sampleStatic',facecolor = 'xkcd:grey')
plt.show()
