import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import animation
import matplotlib.cm as cm
def v_disparity(img):
    histWidth = int(np.max(img))+1
    vDisparity = np.zeros((img.shape[0],histWidth))

    for i,row in enumerate(img):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        vDisparity[i] = hist
    return vDisparity

def u_disparity(img):
    histWidth = int(np.max(img)+1)
    uDisparity = np.zeros((img.T.shape[0],histWidth))

    for i,row in enumerate(img.T):
        hist = np.zeros(histWidth)
        #print hist.shape
        for pixel in row:
            #print int(pixel)
            hist[int(pixel)] += 1
        uDisparity[i] = hist
    uDisparity.T[0]+=1
    return uDisparity.T

def simulated_image():
    image = np.zeros((16,16))
    for i, imageRow in enumerate(image[int(image.shape[0]/2):image.shape[0]+1]):
        imageRow += int(image.shape[0]/2) - i
        #print(int(image.shape[0]/2) - i)
        image[i] += 8
    for i in range(4,12):
        for j in range(4,image.shape[1]-4):
            image[i,j] = 4
    return(image)

image = simulated_image()

rectangle = Rectangle((0,image.shape[0]-6.5), width = image.shape[1]-1, height = 1,facecolor='none',edgecolor='w')

vDisp = v_disparity(image)
uDisp = u_disparity(image)
def init():

    ax.imshow(image,cmap='viridis_r')
    return[]
def v_trace_rectangle(*args):
    ax.clear()
    ax3.clear()
    ax1.title.set_text('V-Disparity Map')
    ax.title.set_text('Distance Image')
    ax3.title.set_text('Histogram of Distance Values at Index')
    ax3.set_xlim(0,int(image.max()+1))
    ax3.set_ylim(0,image.shape[1])
    ax3.set_facecolor(colors[0])
    ax3.set_aspect(vDisp.shape[1]/vDisp.shape[0])
    #ax1.set_aspect(image.shape[1]/image.max())
    ax3.set_aspect(ax1.get_aspect())

    ax.imshow(image,cmap='viridis_r')

    i = args[0]
    ani_vDisp = np.copy(vDisp)
    ani_vDisp[0:vDisp.shape[0]-i-1] = 0
    ax1.imshow(ani_vDisp)
    #hist = np.histogram(image[-i-1],bins = range(int(image.max())+1))
    n, bins, patches = ax3.hist(image[-i-1],range(int(image.max())))
    for patchNumber,patch in enumerate(patches):
        #print(n[i])
        patch.set_color(colors[int(n[patchNumber])])
    rectangle = ax.add_patch(Rectangle((0,image.shape[0]-1.5-i), width = image.shape[1]-1, height = 1,facecolor='none',edgecolor='w'))

    return(rectangle,)

def u_trace_rectangle(*args):
    ax.clear()
    ax.title.set_text('Distance Image')
    ax.imshow(image,cmap='viridis_r')
    ax3.clear()
    ax3.title.set_text('Histogram of Distance Values at Index')
    ax3.set_ylim(1,image.max())
    ax3.set_xlim(0,int(image.shape[1])-1)
    ax3.set_aspect(ax4.get_aspect())
    ax3.set_xticklabels(['a', 'b', 'c', 'd'])
    i = args[0]
    rectangle = ax.add_patch(Rectangle((-0.5+i,0), width = 1, height =image.shape[1]-1,facecolor='none',edgecolor='w'))
    ani_uDisp = np.copy(uDisp)
    ani_uDisp.T[i+1:] = 0
    ax4.imshow(ani_uDisp,cmap = 'viridis')
    ax4.title.set_text('U-Disparity Map')
    #hist = np.histogram(image.T[i],bins = range(int(image.max())+1))
    n, bins, patches = ax3.hist(image.T[i],range(int(image.max()+1)),orientation = 'horizontal')
    ax3.set_xticklabels(bins)
    #colors = cm.viridis_r(np.linspace(0, 1, int(image.shape[1]+1)))
    #ax3.set_xlim(0,n.max())
    for patchNumber,patch in enumerate(patches):
        #print(n[patchNumber])
        patch.set_color(colors[int(n[patchNumber])+1])
        #patch.set_color(colors[9])
    ax3.invert_yaxis()
    return rectangle,

#plt.imshow(uDisp)
colors = cm.viridis(np.linspace(0, 2, int(image.shape[1])))
fig = plt.figure(facecolor='xkcd:grey')

ax1 = fig.add_subplot(221)

ax1.set_aspect(vDisp.shape[0]/vDisp.shape[1])
#ax1.title.set_text('V-Disparity Map')
ax1.axis('off')
#ax1.imshow(vDisp)
ax4 = fig.add_subplot(224)
#ax4.imshow(uDisp)
#ax4.axis('off')
ax=fig.add_subplot(222)
ax3 = fig.add_subplot(223,facecolor=colors[0])
ax3.axis('off')

#ax1.imshow(np.zeros((vDisp.shape[0],vDisp.shape[1])))
#ax1.title.set_text('V-Disparity Map')
ax.title.set_text('Distance Image')

plt.tight_layout()
anim = animation.FuncAnimation(fig, u_trace_rectangle, init_func=init,
                               frames=image.shape[1], interval=1000, blit=False)
#anim.save('uDispMethod.gif', dpi=200, writer='imagemagick',savefig_kwargs = {'facecolor':'xkcd:grey'})
#anim.to_html5_video()
plt.show()
