import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


fig = plt.figure()
fig.set_facecolor('rebeccapurple')
ax = fig.add_subplot(111,projection='3d',aspect=1)
ax.set_zlim(0,4)

# Playing around with colors
ax.set_facecolor('rebeccapurple')
ax.set_yticklabels('')
ax.set_xticklabels('')
ax.set_zticklabels('')
#ax.set_xticks([])
#ax.set_yticks([])
#ax.set_zticks([])
ax.w_xaxis.set_pane_color((0,0,0))
ax.w_zaxis.set_pane_color((0,.3,0.3))
ax.w_yaxis.set_pane_color((1,0.1,0.1))



m1Width = 9
m1Height = 9
# Generate coordinates for mesh grid
x1 = np.arange(-m1Width/2,m1Width/2,1)
y1 = np.arange(-m1Height/2,m1Height/2,1)

xx1, yy1 = np.meshgrid(x1,y1,sparse=True)

# Generate random Z data for mesh grid
zz1 = np.random.rand(m1Height, m1Width)

# Create some clusters
zz1[3:6,1:3]+=3
zz1[7:9,7:9]+=2

# Create [x y z] array for the points in the mesh grid
xGrid = np.copy(xx1)
yGrid = np.copy(yy1)
for i in range(m1Height-1):
    xGrid = np.vstack((xGrid,xx1))
for j in range(m1Width-1):
    yGrid = np.hstack((yGrid,yy1))
xs= xGrid.flatten()
ys= yGrid.flatten()
zs= zz1.flatten()
X = np.array([xs,ys,zs]).T

# DBSCAN clustering algorithm
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

db = DBSCAN(eps=1.5, min_samples=3).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
clusters = []
unique_labels = set(labels)
colors = [plt.cm.Vega20(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    ax.plot(xy[:, 0], xy[:, 1], xy[:, 2], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=4)

    clusters.append(xy)
    xy = X[class_member_mask & ~core_samples_mask]
    ax.plot(xy[:, 0], xy[:, 1], xy[:,2], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=4)

zClusters = np.zeros((len(clusters),m1Height,m1Width))
for cIndex,cluster in enumerate(clusters):

    for point in cluster:
        if point[1] in x1:
            coord=[np.where(x1 == point[1])[0][0],np.where(y1 == point[0])[0][0]]
            zClusters[cIndex][coord[0],coord[1]] = zz1[coord[0],coord[1]]



print xx1.shape
print yy1.shape
for zCluster in zClusters:
    #print zCluster
    roi = np.where(zCluster > 0)
    #print roi
    dimX = [np.min(roi[0]),np.max(roi[0])+1]
    dimY = [np.min(roi[1]),np.max(roi[1])+1]
    #print dimX
    #print dimY

    print(np.array([xx1[0][dimX[0]:dimX[1]],])).shape
    print(yy1[dimY[0]:dimY[1]]).shape
    print zCluster[dimY[0]:dimY[1],dimX[0]:dimX[1]].shape
    #print zCluster[np.min(roi[0]):np.max(roi[0])+1,np.min(roi[1]):np.max(roi[1])+1]
    #ax.plot_surface(np.array([xx1[0][dimX[0]:dimX[1]],]),yy1[dimY[0]:dimY[1]],zCluster[dimY[0]:dimY[1],dimX[0]:dimX[1]],cmap='cubehelix')

# Plot the surface
#ax.plot_surface(xx1,yy1,zz1,cmap='cubehelix')
#ax.plot(xGrid.flatten(),yGrid.flatten(),zz1.flatten(),'o', markerfacecolor='g',markeredgecolor='k',markersize=1)
#ax.plot(X[:,0],X[:,1],X[:,2],'o', markerfacecolor='g',markeredgecolor='k',markersize=1)
#fig.savefig('meshClusters.png',facecolor='rebeccapurple')
plt.tight_layout()
#plt.show()
