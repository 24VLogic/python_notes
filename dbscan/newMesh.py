import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure()
fig.set_facecolor('rebeccapurple')
ax = fig.add_subplot(111,projection='3d',aspect=1)
ax.set_zlim(0,5)


m1Width =8
m1Height = 12

x1 = np.arange(-m1Width/2,m1Width/2,1)
y1 = np.arange(-m1Height/2,m1Height/2,1)

xx1, yy1 = np.meshgrid(x1,y1)

#zz1 = np.random.rand(m1Width,m1Height)
zz1 = np.random.rand(m1Height,m1Width)
zz1[0:4,0:4] += 2
zz1[0:3,4:6] += 2
print zz1
# Create some clusters

'''
xGrid = np.copy(xx1)
yGrid = np.copy(yy1)

for j in range(m1Height-1):
    xGrid=np.vstack((xGrid,xx1))

for i in range(m1Width-1):
    yGrid=np.hstack((yGrid,yy1))
'''

X = np.array([xx1.flatten(),yy1.flatten(),zz1.flatten()]).T

# DBSCAN clustering algorithm
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

db = DBSCAN(eps=1.5, min_samples=3).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_)
clusters = []
unique_labels = set(labels)
colors = [plt.cm.Vega20(each)
          for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = (labels == k)

    xy = X[class_member_mask & core_samples_mask]
    print xy
    ax.plot(  xy[:, 0], xy[:, 1], xy[:, 2], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=4)



    clusters.append(xy)
    xy = X[class_member_mask & ~core_samples_mask]
    ax.plot( xy[:, 0],xy[:, 1],  xy[:,2], 'o', markerfacecolor=tuple(col),
             markeredgecolor='k', markersize=4)
for cluster in clusters:
    print cluster.shape
    if cluster.shape[0] >= 3:
        ax.plot_trisurf(cluster[:, 0], cluster[:, 1], cluster[:, 2])
'''
zClusters = []
rois = []
for cluster in clusters:
    matrix = np.zeros((m1Width,m1Height)).T
    cX = cluster[:,0]
    cY = cluster[:,1]
    cZ = cluster[:,2]
    #print cX, cY, cZ
    if cX != []:

        roi = [[np.min(cY),np.max(cY)],[np.min(cX),np.max(cX)]]
        yr1= np.where(y1==roi[0][0])[0][0]
        yr2= np.where(y1==roi[0][1])[0][0]
        xr1= np.where(x1==roi[1][0])[0][0]
        xr2= np.where(x1==roi[1][1])[0][0]
        for point in cluster:
            xCoord = np.where(x1==point[0])
            yCoord = np.where(y1==point[1])
            matrix[yCoord,xCoord] = point[2]
        zClusters.append(matrix)
        #print yr1,yr2,xr1,xr2

        print matrix[yr1:yr2+1,xr1:xr2+1]
        print matrix
        print x1[xr1:xr2+1]
        print y1[yr1:yr2+1]

        mx,my = np.meshgrid(x1[xr1:xr2+1],y1[yr1:yr2+1])
        matz = matrix[yr1:yr2+1,xr1:xr2+1]
        ax.plot_surface(mx,my,matz)
'''
#print zClusters
plt.show()
