import numpy as np

def get_paramesh(xVector,yVector):
    uLine = xVector
    vLine = yVector
    xCoords = uLine.T[0]
    yCoords = uLine.T[1]
    for i, xVal in enumerate(vLine[1:]):
        xCoords= np.vstack((uLine.T[0]+vLine.T[0][i+1]-vLine.T[0][0],xCoords))
        yCoords= np.vstack((uLine.T[1]+vLine.T[1][i+1]-vLine.T[1][0],yCoords))
    yCoords = yCoords
    return({'x':xCoords,'y':yCoords})

def main():
    import matplotlib.pyplot as plt
    uLine = np.array([np.linspace(3,10,num = 3),np.linspace(3,7, num = 3)]).T
    vLine = np.array([np.linspace(3,5,num = 5),np.linspace(3,14, num = 5)]).T
    plt.figure()
    paramesh = get_paramesh(uLine,vLine)
    plt.plot(paramesh['x'].flatten(),paramesh['y'].flatten(),'o')
    plt.plot(uLine[:,0],uLine[:,1])
    plt.plot(vLine[:,0],vLine[:,1])
    plt.savefig('example2d')
    test_3d()
    plt.show()
def test_3d():
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.set_position([0,0,1,1])
    ax.set_zlim(0,4)
    width = 15
    height = 18
    uLine = np.array([np.linspace(3,10,num = width),np.linspace(3,7, num = width)]).T
    vLine = np.array([np.linspace(3,5,num = height),np.linspace(3,14, num = height)]).T
    paramesh = get_paramesh(uLine,vLine)
    zRandom = np.random.rand(height,width)
    ax.plot_surface(paramesh['x'],paramesh['y'],zRandom,cmap='cubehelix')
    uLine = np.array([np.linspace(4,7,num = width),np.linspace(5,7, num = width)]).T
    vLine = np.array([np.linspace(4,5,num = height),np.linspace(5,14, num = height)]).T
    paramesh = get_paramesh(uLine,vLine)
    zClimbElement = np.linspace(3,2,num = width)
    zClimb = np.copy(zClimbElement)
    for i in range(height-1):
        zClimb = np.vstack((zClimb,zClimbElement))
    ax.plot_surface(paramesh['x'],paramesh['y'],zClimb,cmap = 'magma')

    height = 12
    width = 12
    zStep = np.zeros((height,width))
    zStep += 1
    zStep[3:6,3:6] += 0.5
    uLine = np.array([np.linspace(4,8,num = width),np.linspace(6,6, num = width)]).T
    vLine = np.array([np.linspace(4,4,num = height),np.linspace(4,8, num = height)]).T
    paramesh = get_paramesh(uLine,vLine)
    ax.plot_wireframe(paramesh['x'],paramesh['y'],zStep)
    plt.savefig('example3d')
if __name__ == "__main__":
    main()
    #test_3d()
