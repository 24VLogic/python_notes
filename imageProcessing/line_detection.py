import numpy as np
import matplotlib.pyplot as plt
import cv2
from skimage.morphology import skeletonize
class testLine():
    def __init__(self):
        self.endPoints = np.zeros(2)
        self.m = None
        self.b = None
        self.xy = None
    def set_endPoints(self, endpoints,n = 1000):
        self.endPoints = endpoints
        xRange = np.linspace(self.endPoints[0][0],self.endPoints[0][1],num = n)
        yRange = np.linspace(self.endPoints[1][0],self.endPoints[1][1],num = n)
        self.xy = np.array([
            xRange,
            yRange
            ]).T.astype('uint8')

def main():
    image = np.zeros((64,64),dtype = 'uint8')
    endPoints1 = [[0,30],[45,63]]
    endPoints2 = [[20,20], [3, 48]]
    endPoints3 = [[63,63-25],[0,45]]
    newLine1 = testLine()
    newLine1.set_endPoints(endPoints1)
    newLine2 = testLine()
    newLine2.set_endPoints(endPoints2)
    newLine3 = testLine()
    newLine3.set_endPoints(endPoints3)
    '''for point in newLine1.xy:
        #x,y = point,
        image[point[0],point[1]] = 1'''
    for point in newLine2.xy:
        #x,y = point,
        image[point[0],point[1]] = 1
    for point in newLine3.xy:
        #x,y = point,
        image[point[0],point[1]] = 1
    ret,thresh1 = cv2.threshold(image,0,1,cv2.THRESH_BINARY)
    #print (thresh1)
    minLineLength = 20
    maxLineGap = 1
    skel = skeletonize(image).astype('uint8')
    #edges = cv2.Canny
    lines = cv2.HoughLinesP(image,1,np.pi/720,30,minLineLength,maxLineGap)
    skLines = cv2.HoughLinesP(skel,1,np.pi/1080,10,minLineLength,maxLineGap)
    #print(lines)
    fig = plt.figure(facecolor = 'xkcd:teal')
    ax1 = fig.add_subplot(221)
    ax1Img = ax1.imshow(image)
    ax1.axis('off')
    ax2 = fig.add_subplot(222,aspect = 'equal')
    ax2.set_xlim(0,64)
    ax2.set_ylim(64,0)
    #ax2.axis('off')
    for line in lines:
        for x1,y1,x2,y2 in line:
            ax2.plot([x1,x2],[y1,y2])
    ax3 = fig.add_subplot(223,aspect = 'equal')
    ax3.set_xlim(0,64)
    ax3.set_ylim(64,0)
    ax3.axis('off')
    ax3.imshow(skel)
    ax4 = fig.add_subplot(224,aspect = 'equal')
    ax4.set_xlim(0,64)
    ax4.set_ylim(64,0)
    #ax4.axis('off')
    for line in skLines:
        for x1,y1,x2,y2 in line:
            ax4.plot([x1,x2],[y1,y2])
    #ax2Img = ax2.imshow(image)
    #plt.savefig('line_detection')
    plt.show()

if __name__ == "__main__":
    main()
